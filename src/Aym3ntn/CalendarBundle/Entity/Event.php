<?php

namespace Aym3ntn\CalendarBundle\Entity;


/**
 * Event
 *
 */
class Event
{
    private $id;

    private $title;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    private $url;

    private $allDay = false;

    private $backgroundColor;

    private $borderColor;

    function __construct($id ,$title, $start, $end, $allDay, $backgroundColor, $borderColor, $url)
    {
        $this->allDay = $allDay;
        $this->backgroundColor = $backgroundColor;
        $this->borderColor = $borderColor;
        $this->end = $end;
        $this->id = $id;
        $this->start = $start;
        $this->title = $title;
        $this->url = $url;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    public function getAllDay()
    {
        return $this->allDay;
    }

    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    public function setBorderColor($borderColor)
    {
        $this->borderColor = $borderColor;

        return $this;
    }

    public function getBorderColor()
    {
        return $this->borderColor;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    function __toString()
    {
        /* end: "'.$this->getEnd()->format('Y-m-d H:m').'",*/

        return ' title: \''.$this->getTitle().'\',
                 start: "'.$this->getStart()->format('Y-m-d H:i').'",
                 allDay: false,
                 backgroundColor: "'.$this->getBackgroundColor().'",
                 borderColor: "'.$this->getBorderColor().'",
                 url:"'.$this->getUrl().'"';
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
