<?php

namespace Aym3ntn\CalendarBundle\Controller;

use Aym3ntn\CalendarBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Aym3ntn\MedecinBundle\Entity\Medecin;
use Aym3ntn\MedecinBundle\Form\MedecinType;

/**
 * Rdz controller.
 *
 */
class RdzController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tasks = $em->getRepository('TacheBundle:Tache')->findByOwner($this->getUser());

        /*
                        title: 'Lunch',
                        start: new Date(2014,25,8,19,0),
                        end: new Date(2014,25,8,19,0),
                        allDay: false,
                        backgroundColor: "#00c0ef", //Info (aqua)
                        borderColor: "#00c0ef" //Info (aqua)
         * */

        $tasks1 = $em->getRepository('TacheBundle:Tache')->getTacheByMember($this->getUser());

        $tasks = array_merge($tasks, $tasks1);
        $events = [];
        foreach($tasks as $i => $task){
            if( $task->getOwner() == $this->getUser() )
                $color = '#f39c12';
            else
                $color = '#00c0ef';

            $events[$i] = ',{'.(new Event($i, $task->getDescr(), $task->getDate(), $task->getDate(), false, $color, $color, $this->generateUrl('admin_task_show', array('id'=>$task->getId()))))->__toString().'}';
        }

        $events[0][0] = ' ';
        return $this->render('CalendarBundle:Rdz:index.html.twig', array( 'events' => $events
        ));
    }
}
