<?php

namespace Aym3ntn\TacheBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TacheRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TacheRepository extends EntityRepository
{
    public function getTacheByMember($member){
        $qb = $this->createQueryBuilder('q')
            ->join('q.members','m','WITH', 'm.id = :member')
            ->where('q.status = 3')
            ->setParameter('member', $member);
        return $qb->getQuery()->getResult();
    }
}
