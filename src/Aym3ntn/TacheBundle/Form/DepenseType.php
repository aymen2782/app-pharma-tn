<?php

namespace Aym3ntn\TacheBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DepenseType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('etiquette')
            ->add('qte')
            ->add('prix')
            ->add('type', 'choice', array(
                'choices' => array('rl' => 'Réel', 'Prv' => 'Prévionnelle')
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aym3ntn\TacheBundle\Entity\Depense'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aym3ntn_tachebundle_depense';
    }
}
