<?php

namespace Aym3ntn\TacheBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

class TacheType extends AbstractType
{
    private $securityContext;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'datetime', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'required' => true,
            ))
            ->add('descr', null, array(
                'required' => true,
            ))
            ->add('medecins', null, array(
                    'multiple'  => true,
                    'required' => true,
            ))
            ->add('members', 'entity', array(
                'class' => 'Aym3ntn\UserBundle\Entity\User',
                'required' => true,
                'multiple' => true,
                'empty_value' => 'Vous devez choisir le secteur auquel le medecin appartient.',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->where('s.id != :id ')
                            ->setParameter('id', $this->securityContext->getToken()->getUser());
                    }
            ))
            ->add('type','entity', array(
                'class' => 'Aym3ntn\TacheBundle\Entity\TypeTache',
                'property' => 'etiquette',
                'empty_value' => 'Vous devez choisir un type!',
                'required' => true,
            ))
            ->add('lieu', null, array(
                'property' => 'etiquette',
                'empty_value' => 'Vous devez choisir un lieu!',
                'required' => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aym3ntn\TacheBundle\Entity\Tache',
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => 'task_item',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aym3ntn_tachebundle_tache';
    }
}
